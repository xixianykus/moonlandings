## The git branches

**These branches do NOT include the newer version of the site which is in html-extras/moonlandings2**

This is about the different branches

Currently the MASTER branch was merged from NewStyle01.

branch-1 is the last and best version of the white one with responsive design

original is an older more basic version with simpler code



## To use VIM (Vi IMproved) 

press escape to exit *insert* mode

Press colon : to enter *command* line mode

Then qw to quit and save

To do more editing press i to enter *insert* mode
